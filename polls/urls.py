from django.urls import path

from polls.views import index, ready

urlpatterns = [
    path('', index, name='index'),
    path('ready', ready, name='ready'),
]
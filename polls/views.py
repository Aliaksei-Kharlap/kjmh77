
from django.shortcuts import render, redirect
from .forms import InputForm
from .models import InputModel
import json

def index(request):
    if request.method == 'POST':
        form = InputForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            data = json.dumps(name, ensure_ascii=False)
            InputModel.objects.create(data=data,field='name')

            for count in range(1,len(request.POST)-1):
                queryset = 'name' + str(count)
                input_value = request.POST[queryset]

                if input_value:
                    data = json.dumps(input_value, ensure_ascii=False)
                    field = json.dumps(queryset)
                    InputModel.objects.create(data=data, field=field)

                count += 1

        return redirect('ready')

    else:
        form = InputForm()

    return render(request, 'server/index.html', {'form': form})

def ready(request):
    queryset = InputModel.objects.all().values('id','field','data')
    json_lst = list(queryset)
    return render(request, 'server/ready.html', {'json_lst' : json_lst})